import { ApplicationInsights, SeverityLevel } from "@microsoft/applicationinsights-web";

export class LoggerService {

  constructor(
    private readonly applicationInsights: ApplicationInsights
  ) {
  }

  public log(message: string, properties: {[key: string]: string}, severityLevel: SeverityLevel = SeverityLevel.Error) {
    this.applicationInsights.trackTrace({ message, severityLevel, properties });
  }
}
