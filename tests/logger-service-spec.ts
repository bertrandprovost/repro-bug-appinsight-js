import { ApplicationInsights, SeverityLevel } from "@microsoft/applicationinsights-web";
import { LoggerService } from "../src";

describe("Logger Service", () => {
  let logger: LoggerService;
  let applicationInsights: jasmine.SpyObj<ApplicationInsights>;

  function castMock<T>(obj: jasmine.SpyObj<T>): T {
    return obj;
  }

  beforeEach(() => {
    applicationInsights = jasmine.createSpyObj<ApplicationInsights>("appInsights", [
      "trackTrace",
      "trackException",
      "flush"
    ]);
    logger = new LoggerService(castMock(applicationInsights));
  });

  describe("when log is called", () => {
    it("should call the Application Insights API", () => {
      const context = {
        some: "context"
      };

      logger.log("some message", context);

      expect(applicationInsights.trackTrace).toHaveBeenCalledWith({
        message: "some message",
        properties: context,
        severity: SeverityLevel.Error
      });
    });
  });
});
