module.exports = function(config) {
  config.set({
    plugins: [
      "karma-jasmine",
      "karma-chrome-launcher",
      "karma-tfs-reporter",
      "karma-webpack"
    ],
    frameworks: ["jasmine"],
    files: ["./tests/index.ts"],
    preprocessors: {
      "./tests/index.ts": ["webpack"]
    },
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    browsers: ["ChromeHeadless"],
    reporters: ["progress", "tfs"],
    tfsReporter: {
      outputDir: "../../test-results",
      outputFile: "mediaclip-hub-shared.xml"
    },
    webpack: {
      mode: "development",
      module: {
        rules: [
          {
            test: /\.tsx?$/,
            use: "ts-loader",
            exclude: /node_modules/
          }
        ]
      },
      resolve: {
        extensions: [".tsx", ".ts"]
      },
      devtool: "inline-source-map"
    },
    webpackMiddleware: {
      stats: "errors-only"
    },
    karmaTypescriptConfig: {
      coverageOptions: {
        instrumentation: false
      }
    }
  });
};
